from flask import Flask
from app.views.status import bp_status
from app.views.users import bp_users
from app.views.address import bp_adresses
from app.models import db, mg
from environs import Env

def create_app():
    env = Env()
    env.read_env()
        
    app = Flask(__name__)
    
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = env.bool('SQLALCHEMY_TRACK_MODIFICATIONS')
    app.config['SQLALCHEMY_DATABASE_URI'] = env.str('SQLALCHEMY_DATABASE_URI')
    
    db.init_app(app)
    mg.init_app(app, db)
    
    app.register_blueprint(bp_status)
    app.register_blueprint(bp_users)
    app.register_blueprint(bp_adresses)

    return app
