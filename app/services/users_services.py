from app.models import User
from typing import List
from app.services.address_services import serialize_address_list

def serialize_user(user: User) -> dict:
    return {
        'id': user.id,
        'name': user.name,
        'surname': user.surname,
        'document': user.document,
        'adresses' : serialize_address_list(user.adresses)
    }


def serialize_user_list(user_list: List[User]) -> List[dict]:
    return [serialize_user(user) for user in user_list]
